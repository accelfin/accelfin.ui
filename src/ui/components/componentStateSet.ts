interface ComponentStateSet {
    componentFactoryKey: string;
    componentsState: Array<any>;
}

export default ComponentStateSet;