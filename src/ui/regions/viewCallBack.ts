import ModelBase from '../modelBase';
type ViewCallBack = (model: ModelBase, viewKey?: string) => void;

export default ViewCallBack;