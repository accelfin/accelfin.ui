import RegionItem from '../../../regionItem';

interface SelectedTileChangedEvent {
    selectedItem: RegionItem;
}

export default SelectedTileChangedEvent;