import MultiTileRegionModel from '../model/multiTileRegionModel';
import ViewBaseProps from '../../../viewBaseProps';

interface MultiTileRegionViewProps extends ViewBaseProps<MultiTileRegionModel> {
    className?: string;
}

export default MultiTileRegionViewProps;
