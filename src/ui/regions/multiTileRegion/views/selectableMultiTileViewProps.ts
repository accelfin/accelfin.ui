import MultiTileRegionModel from '../model/multiTileRegionModel';
import ViewBaseProps from '../../../viewBaseProps';

interface SelectableMultiTileViewProps extends ViewBaseProps<MultiTileRegionModel> {
    className?: string;
}
export default SelectableMultiTileViewProps;