import SingleItemRegionModel from '../model/singleItemRegionModel';
import ViewBaseProps from '../../../viewBaseProps';

interface SingleItemRegionViewProps extends ViewBaseProps<SingleItemRegionModel> {
    className?: string;
}

export default SingleItemRegionViewProps;